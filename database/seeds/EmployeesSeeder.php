<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert(
            [
                [
                    'manager' => '1',
                    'employee' => '2',
                    'created_at' => date('Y-m-d G:i:s')
                    ],
                   
            ]
                );
    }
}
