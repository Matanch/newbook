<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Eyal Keren',
                    'email' => 'eyalker@gmail.com',
                    'password' => '12345678',
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'Ori Hakak',
                    'email' => 'orihk@gmail.com',
                    'password' => '12345678',
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                ]
            ]
        );
       
    }
}
