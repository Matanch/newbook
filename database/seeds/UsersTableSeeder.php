<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Amir mesilate',
                    'email' => 'Amir@gmail.com',
                    'password' =>Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s')
                    ],
                    [
                     'name' => 'Eyal keren',
                     'email' => 'Eyal@gmail.com',
                     'password' =>Hash::make('12345678'),
                     'created_at' => date('Y-m-d G:i:s')
                     ]
            ]
    
        );
    }
}
