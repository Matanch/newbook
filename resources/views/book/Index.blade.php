@extends('layouts.app')
@section('content')

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class='container'>
    <br/><br/>
      <table class="table table-bordered">
          <thead class="thead-dark">
            <tr>
              <th scope="col">id</th>
              <th scope="col">Book title</th>
              <th scope="col">Author name</th>
              <th scope="col">Created at</th>
              <th scope="col">Updated at</th>
              <th scope="col">Status</th>
       


            </tr>
          </thead>
      
          <tbody>
          @foreach($books as $book)
            <tr>
                <td><a href = "{{route('books.edit' , $book->id)}}">{{$book->id}} </td>
                <td><a href = "{{route('books.edit' , $book->id)}}">{{$book->title}}</td>
                <td>{{$book->author}}</td>
                <td>{{$book->created_at}}</td> 
                <td>{{$book->updated_at}}</td>
                <td>{{$book->status}}
                @if ($book->status)
                <input type = 'checkbox' id ="{{$book->id}}" checked>
                @else
               <input type = 'checkbox' id ="{{$book->id}}">
               @endif  
                </td>
            </tr>
            @endforeach

          </tbody>
      </table>
          <div class ="container">
        <div class="col-0  offset-0">
           <a href="{{route('books.create')}}" class=" btn btn-secondary">Add a book to your list</a>
       </div>
       @can('manager')
       <a href="{{route('books.create')}}">Create New todo </a>
       @endcan
</div>
@endsection


<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: {{url('books')}} + '/' + event.target.id ,
                   dataType:'json' ,
                   type:'put',
                   contentType:'application/json',
                   data: JSON.stringify{'status':event.target.checked, _token:'{{csrf_token()}}'},
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  
