<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use App\Book;
use App\User;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$id=1;
        $id=Auth::id();
        if (Gate::denies('manager')) {
            $boss = DB::table('employees')->where('employee',$id)->first();
            $id = $boss->manager;
        }
        $books =User::find($id)->books;
        return view('book.index' , compact('books'));

        //return view('book.index'  , ['books'=>$books]);
  
 




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create books..");
        }
 
       return view('book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        $book=new Book();
        //$id = 1;
        $id=Auth::id();//the idea of the current user
        $book->title=$request->title;
        $book->user_id=$id;
        $book ->author=$request->author;
        $book->created_at =now()->toDateTimeString();;
        $book->status=0;
        $book->save();
        return redirect('books');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
       $book = Book::find($id);
       $id = Auth::id(); //the idea of the current user
        return view('book.edit' , compact('book'));

        
     
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //only if this todo belongs to user
       $book = Book::findOrFail($id);
       //employees are not allowed to change the title 
        if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit books..");
        }   
         //make sure the todo belongs to the logged in user
       if(!$book->user->id == Auth::id()) return(redirect('books'));
  
       //test if title is dirty
      
       $book->update($request->except(['_token']));
  
       if($request->ajax()){
           return Response::json(array('result' => 'success1','status' => $request->status ), 200);   
       } else {          
           return redirect('books');           
       }

        // $book = Book::find($id);
        // $book -> update($request->all());
        //  return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        $book = Book::find($id);
        #we can right also $book -> delete($id);
        $book -> delete();
        $id = Auth::id(); //the idea of the current user
        $book->title = $request->title;
        $book->user_id = $id;
        $book->status = 0;
        $book->save();
        return redirect('books');

      
      
 
    }
}