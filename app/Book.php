<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Book extends Model
{
 //database fields to be allwed for massive assigment
 protected $fillable =[
    'title','author', 'status',
];

    /**
     * Get the user that owns the books.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

